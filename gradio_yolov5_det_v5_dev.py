# Gradio YOLOv5 Det v0.5 开发版
# 创建人：曾逸夫
# 创建时间：2022-06-16
# 功能描述：热重载测试
# gradio gradio_yolov5_det_v5_dev.py gyd
# 参考：https://github.com/gradio-app/gradio/blob/main/guides/developing_faster_with_reload_mode.md
# 要求：gradio>=3.0.17

import csv
import gc
import json
import os
import sys
from collections import Counter
from pathlib import Path

import cv2
import gradio as gr
import numpy as np
import pandas as pd
import plotly.express as px
import torch
import yaml
from PIL import Image, ImageDraw, ImageFont

from util.fonts_opt import is_fonts
from util.pdf_opt import pdf_generate

ROOT_PATH = sys.path[0]  # 根目录

# yolov5路径
yolov5_path = "ultralytics/yolov5"

# 本地模型路径
local_model_path = f"{ROOT_PATH}/models"

# Gradio YOLOv5 Det版本
GYD_VERSION = "Gradio YOLOv5 Det v0.5"

# 模型名称临时变量
model_name_tmp = ""

# 设备临时变量
device_tmp = ""

# 文件后缀
suffix_list = [".csv", ".yaml"]

# 字体大小
FONTSIZE = 25

# 目标尺寸
obj_style = ["小目标", "中目标", "大目标"]


# yaml文件解析
def yaml_parse(file_path):
    return yaml.safe_load(open(file_path, encoding="utf-8").read())


# yaml csv 文件解析
def yaml_csv(file_path, file_tag):
    file_suffix = Path(file_path).suffix
    if file_suffix == suffix_list[0]:
        # 模型名称
        file_names = [i[0] for i in list(csv.reader(open(file_path)))]  # csv版
    elif file_suffix == suffix_list[1]:
        # 模型名称
        file_names = yaml_parse(file_path).get(file_tag)  # yaml版
    else:
        print(f"{file_path}格式不正确！程序退出！")
        sys.exit()

    return file_names


#  模型加载
def model_loading(model_name, device, opt=[]):

    # 加载本地模型
    try:
        torch.hub._validate_not_a_forked_repo = lambda a, b, c: True
        model = torch.hub.load(
            yolov5_path,
            "custom",
            path=f"{local_model_path}/{model_name}",
            device=device,
            force_reload=[True if "refresh_yolov5" in opt else False][0],
            _verbose=True,
        )
    except Exception as e:
        print("模型加载失败！")
        print(e)
        return False
    else:
        print(f"🚀 欢迎使用{GYD_VERSION}，{model_name}加载成功！")

    return model


# 检测信息
def export_json(results, img_size):

    return [[{
        "ID": i,
        "CLASS": int(result[i][5]),
        "CLASS_NAME": model_cls_name_cp[int(result[i][5])],
        "BOUNDING_BOX": {
            "XMIN": round(result[i][:4].tolist()[0], 6),
            "YMIN": round(result[i][:4].tolist()[1], 6),
            "XMAX": round(result[i][:4].tolist()[2], 6),
            "YMAX": round(result[i][:4].tolist()[3], 6),},
        "CONF": round(float(result[i][4]), 2),
        "FPS": round(1000 / float(results.t[1]), 2),
        "IMG_WIDTH": img_size[0],
        "IMG_HEIGHT": img_size[1],} for i in range(len(result))] for result in results.xyxyn]


# 检测绘制
def pil_draw(img, score_l, bbox_l, cls_l, cls_index_l, textFont, color_list):

    img_pil = ImageDraw.Draw(img)
    id = 0

    for score, (xmin, ymin, xmax, ymax), label, cls_index in zip(score_l, bbox_l, cls_l, cls_index_l):

        img_pil.rectangle([xmin, ymin, xmax, ymax], fill=None, outline=color_list[cls_index])  # 边界框
        countdown_msg = f"{id}-{label} {score:.2f}"
        text_w, text_h = textFont.getsize(countdown_msg)  # 标签尺寸

        img_pil.rectangle(
            (xmin, ymin, xmin + text_w, ymin + text_h),
            fill=color_list[cls_index],
            outline=color_list[cls_index],
        )  # 标签背景

        img_pil.multiline_text(
            (xmin, ymin),
            countdown_msg,
            fill=(255, 255, 255),
            font=textFont,
            align="center",
        )

        id += 1

    return img


# 标签和边界框颜色设置
def color_set(cls_num):
    color_list = []
    for i in range(cls_num):
        color = tuple(np.random.choice(range(256), size=3))
        # color = ["#"+''.join([random.choice('0123456789ABCDEF') for j in range(6)])]
        color_list.append(color)

    return color_list


# YOLOv5图片检测函数
def yolo_det_img(img, device, model_name, infer_size, conf, iou, max_num, model_cls, opt):

    global model, model_name_tmp, device_tmp

    det_img = img.copy()
    # 目标尺寸个数
    s_obj, m_obj, l_obj = 0, 0, 0

    area_obj_all = []  # 目标面积

    score_det_stat = []  # 置信度统计
    bbox_det_stat = []  # 边界框统计
    cls_det_stat = []  # 类别数量统计
    cls_index_det_stat = []  # 类别索引统计

    if model_name_tmp != model_name:
        # 模型判断，避免反复加载
        model_name_tmp = model_name
        print(f"正在加载模型{model_name_tmp}......")
        model = model_loading(model_name_tmp, device, opt)
    elif device_tmp != device:
        # 设备判断，避免反复加载
        device_tmp = device
        print(f"正在加载模型{model_name_tmp}......")
        model = model_loading(model_name_tmp, device, opt)
    else:
        print(f"正在加载模型{model_name_tmp}......")
        model = model_loading(model_name_tmp, device, opt)

    # -----------模型调参-----------
    model.conf = conf  # NMS 置信度阈值
    model.iou = iou  # NMS IoU阈值
    model.max_det = int(max_num)  # 最大检测框数
    model.classes = model_cls  # 模型类别

    color_list = color_set(len(model_cls_name_cp))  # 设置颜色

    img_size = img.size  # 帧尺寸

    results = model(img, size=infer_size)  # 检测

    # ----------------目标裁剪----------------
    crops = results.crop(save=False)
    img_crops = []
    for i in range(len(crops)):
        img_crops.append(crops[i]["im"][..., ::-1])

    # 数据表
    dataframe = results.pandas().xyxy[0].round(2)

    det_csv = "./Det_Report.csv"
    det_excel = "./Det_Report.xlsx"

    if "csv" in opt:
        dataframe.to_csv(det_csv, index=False)
    else:
        det_csv = None

    if "excel" in opt:
        dataframe.to_excel(det_excel, sheet_name='sheet1', index=False)
    else:
        det_excel = None

    # ----------------加载字体----------------
    yaml_index = cls_name.index(".yaml")
    cls_name_lang = cls_name[yaml_index - 2:yaml_index]

    if cls_name_lang == "zh":
        # 中文
        textFont = ImageFont.truetype(str(f"{ROOT_PATH}/fonts/SimSun.ttf"), size=FONTSIZE)
    elif cls_name_lang in ["en", "ru", "es", "ar"]:
        # 英文、俄语、西班牙语、阿拉伯语
        textFont = ImageFont.truetype(str(f"{ROOT_PATH}/fonts/TimesNewRoman.ttf"), size=FONTSIZE)
    elif cls_name_lang == "ko":
        # 韩语
        textFont = ImageFont.truetype(str(f"{ROOT_PATH}/fonts/malgun.ttf"), size=FONTSIZE)

    for result in results.xyxyn:
        for i in range(len(result)):
            # id = int(i)  # 实例ID
            obj_cls_index = int(result[i][5])  # 类别索引
            cls_index_det_stat.append(obj_cls_index)

            obj_cls = model_cls_name_cp[obj_cls_index]  # 类别
            cls_det_stat.append(obj_cls)

            # ------------边框坐标------------
            x0 = float(result[i][:4].tolist()[0])
            y0 = float(result[i][:4].tolist()[1])
            x1 = float(result[i][:4].tolist()[2])
            y1 = float(result[i][:4].tolist()[3])

            # ------------边框实际坐标------------
            x0 = int(img_size[0] * x0)
            y0 = int(img_size[1] * y0)
            x1 = int(img_size[0] * x1)
            y1 = int(img_size[1] * y1)
            bbox_det_stat.append((x0, y0, x1, y1))

            conf = float(result[i][4])  # 置信度
            score_det_stat.append(conf)

            # fps = f"{(1000 / float(results.t[1])):.2f}"  # FPS

            # ----------加入目标尺寸----------
            w_obj = x1 - x0
            h_obj = y1 - y0
            area_obj = w_obj * h_obj
            area_obj_all.append(area_obj)

    det_img = pil_draw(
        img,
        score_det_stat,
        bbox_det_stat,
        cls_det_stat,
        cls_index_det_stat,
        textFont,
        color_list,
    )

    # ------------JSON生成------------
    det_json = export_json(results, img.size)[0]  # 检测信息
    det_json_format = json.dumps(det_json, sort_keys=False, indent=4, separators=(",", ":"),
                                 ensure_ascii=False)  # JSON格式化
    if "json" not in opt:
        det_json = None

    # --------------PDF生成--------------
    report = "./Det_Report.pdf"
    if "pdf" in opt:
        pdf_generate(f"{det_json_format}", report, GYD_VERSION)
    else:
        report = None

    # --------------目标尺寸计算--------------
    for i in range(len(area_obj_all)):
        if (0 < area_obj_all[i] <= 32 ** 2):
            s_obj = s_obj + 1
        elif (32 ** 2 < area_obj_all[i] <= 96 ** 2):
            m_obj = m_obj + 1
        elif (area_obj_all[i] > 96 ** 2):
            l_obj = l_obj + 1

    sml_obj_total = s_obj + m_obj + l_obj
    objSize_dict = {}
    # objSize_dict = {obj_style[i]: [s_obj, m_obj, l_obj][i] / sml_obj_total for i in range(3)}

    if sml_obj_total == 0:
        objSize_dict = {obj_style[i]: 0 for i in range(3)}
    else:
        objSize_dict = {obj_style[i]: [s_obj, m_obj, l_obj][i] / sml_obj_total for i in range(3)}

    # ------------类别统计------------
    clsRatio_dict = {}
    clsDet_dict = Counter(cls_det_stat)
    clsDet_dict_sum = sum(clsDet_dict.values())

    # for k, v in clsDet_dict.items():
    #     clsRatio_dict[k] = v / clsDet_dict_sum

    if clsDet_dict_sum == 0:
        for k, v in clsDet_dict.items():
            clsRatio_dict[k] = 0
    else:
        for k, v in clsDet_dict.items():
            clsRatio_dict[k] = v / clsDet_dict_sum

    return det_img, img_crops, objSize_dict, clsRatio_dict, dataframe, det_json, report, det_csv, det_excel


# YOLOv5视频检测函数
def yolo_det_video(video, device, model_name, infer_size, conf, iou, max_num, model_cls, opt):

    global model, model_name_tmp, device_tmp

    # 目标尺寸个数
    s_obj, m_obj, l_obj = 0, 0, 0

    area_obj_all = []  # 目标面积
    s_list, m_list, l_list = [], [], []

    score_det_stat = []  # 置信度统计
    bbox_det_stat = []  # 边界框统计
    cls_det_stat = []  # 类别数量统计
    cls_index_det_stat = []  # 类别索引统计

    frame_count = 0  # 帧数

    os.system("""
        if [ -e './output.mp4' ]; then
        rm ./output.mp4
        fi
        """)

    if model_name_tmp != model_name:
        # 模型判断，避免反复加载
        model_name_tmp = model_name
        print(f"正在加载模型{model_name_tmp}......")
        model = model_loading(model_name_tmp, device, opt)
    elif device_tmp != device:
        # 设备判断，避免反复加载
        device_tmp = device
        print(f"正在加载模型{model_name_tmp}......")
        model = model_loading(model_name_tmp, device, opt)
    else:
        print(f"正在加载模型{model_name_tmp}......")
        model = model_loading(model_name_tmp, device, opt)

    # -----------模型调参-----------
    model.conf = conf  # NMS 置信度阈值
    model.iou = iou  # NMS IOU阈值
    model.max_det = int(max_num)  # 最大检测框数
    model.classes = model_cls  # 模型类别

    color_list = color_set(len(model_cls_name_cp))  # 设置颜色

    # ----------------加载字体----------------
    yaml_index = cls_name.index(".yaml")
    cls_name_lang = cls_name[yaml_index - 2:yaml_index]

    if cls_name_lang == "zh":
        # 中文
        textFont = ImageFont.truetype(str(f"{ROOT_PATH}/fonts/SimSun.ttf"), size=FONTSIZE)
    elif cls_name_lang in ["en", "ru", "es", "ar"]:
        # 英文、俄语、西班牙语、阿拉伯语
        textFont = ImageFont.truetype(str(f"{ROOT_PATH}/fonts/TimesNewRoman.ttf"), size=FONTSIZE)
    elif cls_name_lang == "ko":
        # 韩语
        textFont = ImageFont.truetype(str(f"{ROOT_PATH}/fonts/malgun.ttf"), size=FONTSIZE)

    # video->frame
    gc.collect()
    output_video_path = "./output.avi"
    cap = cv2.VideoCapture(video)
    fourcc = cv2.VideoWriter_fourcc(*"I420")  # 编码器

    out = cv2.VideoWriter(output_video_path, fourcc, 30.0, (int(cap.get(3)), int(cap.get(4))))
    while cap.isOpened():
        ret, frame = cap.read()
        # 判断空帧
        if not ret:
            break

        frame_count += 1  # 帧数自增
        results = model(frame, size=infer_size)  # 检测
        h, w, _ = frame.shape  # 帧尺寸
        img_size = (w, h)  # 帧尺寸

        for result in results.xyxyn:
            for i in range(len(result)):
                # id = int(i)  # 实例ID
                obj_cls_index = int(result[i][5])  # 类别索引
                cls_index_det_stat.append(obj_cls_index)

                obj_cls = model_cls_name_cp[obj_cls_index]  # 类别
                cls_det_stat.append(obj_cls)

                # ------------边框坐标------------
                x0 = float(result[i][:4].tolist()[0])
                y0 = float(result[i][:4].tolist()[1])
                x1 = float(result[i][:4].tolist()[2])
                y1 = float(result[i][:4].tolist()[3])

                # ------------边框实际坐标------------
                x0 = int(img_size[0] * x0)
                y0 = int(img_size[1] * y0)
                x1 = int(img_size[0] * x1)
                y1 = int(img_size[1] * y1)
                bbox_det_stat.append((x0, y0, x1, y1))

                conf = float(result[i][4])  # 置信度
                score_det_stat.append(conf)

                # fps = f"{(1000 / float(results.t[1])):.2f}"  # FPS

                # ----------加入目标尺寸----------
                w_obj = x1 - x0
                h_obj = y1 - y0
                area_obj = w_obj * h_obj
                area_obj_all.append(area_obj)

                # frame = cv2.cvtColor(np.asarray(frame), cv2.COLOR_RGB2BGR)

        frame = Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
        frame = pil_draw(
            frame,
            score_det_stat,
            bbox_det_stat,
            cls_det_stat,
            cls_index_det_stat,
            textFont,
            color_list,
        )
        frame = cv2.cvtColor(np.asarray(frame), cv2.COLOR_RGB2BGR)

        # frame->video
        out.write(frame)

        # ----- 清空统计列表 -----
        score_det_stat = []
        bbox_det_stat = []
        cls_det_stat = []
        cls_index_det_stat = []

        # --------------目标尺寸计算--------------
        for i in range(len(area_obj_all)):
            if (0 < area_obj_all[i] <= 32 ** 2):
                s_obj = s_obj + 1
            elif (32 ** 2 < area_obj_all[i] <= 96 ** 2):
                m_obj = m_obj + 1
            elif (area_obj_all[i] > 96 ** 2):
                l_obj = l_obj + 1

        s_list.append(s_obj)
        m_list.append(m_obj)
        l_list.append(l_obj)

        # 目标尺寸个数
        s_obj, m_obj, l_obj = 0, 0, 0
        # 目标面积
        area_obj_all = []

    out.release()
    cap.release()
    # cv2.destroyAllWindows()

    df_objSize = pd.DataFrame({"fID": list(range(frame_count))})
    df_objSize[obj_style[0]] = tuple(s_list)
    df_objSize[obj_style[1]] = tuple(m_list)
    df_objSize[obj_style[2]] = tuple(l_list)

    fig_objSize = px.line(df_objSize, x="fID", y=obj_style, markers=True)
    fig_objSize.update_layout(title="帧数-目标尺寸数", xaxis_title="帧数", yaxis_title="目标尺寸数")

    return output_video_path, fig_objSize


source = "upload"
source_video = "upload"
img_tool = "editor"
nms_conf = 0.5
nms_iou = 0.45
model_name = "yolov5s"
model_cfg = "./model_config/model_name_p5_p6_all.yaml"
cls_name = "./cls_name/cls_name_zh.yaml"
device = "cuda:0"
inference_size = 640
max_detnum = 50
slider_step = 0.05
is_login = False
usr_pwd = ["admin", "admin"]
is_share = False

is_fonts(f"{ROOT_PATH}/fonts")  # 检查字体文件

# 模型加载
model = model_loading(model_name, device)

model_names = yaml_csv(model_cfg, "model_names")  # 模型名称
model_cls_name = yaml_csv(cls_name, "model_cls_name")  # 类别名称

model_cls_name_cp = model_cls_name.copy()  # 类别名称

# -------------------图片模式输入组件-------------------
inputs_img = gr.Image(image_mode="RGB", source=source, tool=img_tool, type="pil", label="原始图片")
inputs_device01 = gr.Radio(choices=["cuda:0", "cpu"], value=device, label="设备")
inputs_model01 = gr.Dropdown(choices=model_names, value=model_name, type="value", label="模型")
inputs_size01 = gr.Radio(choices=[320, 640, 1280], value=inference_size, label="推理尺寸")
input_conf01 = gr.Slider(0, 1, step=slider_step, value=nms_conf, label="置信度阈值")
inputs_iou01 = gr.Slider(0, 1, step=slider_step, value=nms_iou, label="IoU 阈值")
inputs_maxnum01 = gr.Number(value=max_detnum, label="最大检测数")
inputs_clsName01 = gr.CheckboxGroup(choices=model_cls_name, value=model_cls_name, type="index", label="类别")
inputs_opt01 = gr.CheckboxGroup(choices=["refresh_yolov5", "label", "pdf", "json", "csv", "excel"],
                                value=["label", "pdf"],
                                type="value",
                                label="操作")

# -------------------视频模式输入组件-------------------
inputs_video = gr.Video(format="mp4", source=source_video, label="原始视频")  # webcam
inputs_device02 = gr.Radio(choices=["cuda:0", "cpu"], value=device, label="设备")
inputs_model02 = gr.Dropdown(choices=model_names, value=model_name, type="value", label="模型")
inputs_size02 = gr.Radio(choices=[320, 640, 1280], value=inference_size, label="推理尺寸")
input_conf02 = gr.Slider(0, 1, step=slider_step, value=nms_conf, label="置信度阈值")
inputs_iou02 = gr.Slider(0, 1, step=slider_step, value=nms_iou, label="IoU 阈值")
inputs_maxnum02 = gr.Number(value=max_detnum, label="最大检测数")
inputs_clsName02 = gr.CheckboxGroup(choices=model_cls_name, value=model_cls_name, type="index", label="类别")
inputs_opt02 = gr.CheckboxGroup(choices=["refresh_yolov5", "label", "pandas"],
                                value=["label"],
                                type="value",
                                label="操作")

# -------------------图片模式输入参数-------------------
inputs_img_list = [
    inputs_img,  # 输入图片
    inputs_device01,  # 设备
    inputs_model01,  # 模型
    inputs_size01,  # 推理尺寸
    input_conf01,  # 置信度阈值
    inputs_iou01,  # IoU阈值
    inputs_maxnum01,  # 最大检测数
    inputs_clsName01,  # 类别
    inputs_opt01,  # 检测操作
]

# -------------------视频模式输入参数-------------------
inputs_video_list = [
    inputs_video,  # 输入图片
    inputs_device02,  # 设备
    inputs_model02,  # 模型
    inputs_size02,  # 推理尺寸
    input_conf02,  # 置信度阈值
    inputs_iou02,  # IoU阈值
    inputs_maxnum02,  # 最大检测数
    inputs_clsName02,  # 类别
    inputs_opt02,  # 检测操作
]

# -------------------图片模式输出组件-------------------
outputs_img = gr.Image(type="pil", label="检测图片")
outputs_df = gr.Dataframe(max_rows=5, overflow_row_behaviour="paginate", type="pandas", label="检测信息列表")
outputs_crops = gr.Gallery(label="目标裁剪")
outputs_objSize = gr.Label(label="目标尺寸占比统计")
outputs_clsSize = gr.Label(label="类别检测占比统计")
outputs_json = gr.JSON(label="检测信息")
outputs_pdf = gr.File(label="pdf检测报告")
outputs_csv = gr.File(label="csv检测报告")
outputs_excel = gr.File(label="xlsx检测报告")

# -------------------视频模式输出组件-------------------
outputs_video = gr.Video(format='mp4', label="检测视频")
outputs_plot = gr.Plot(label="绘图")

# -------------------图片模式输出参数-------------------
outputs_img_list = [
    outputs_img, outputs_crops, outputs_objSize, outputs_clsSize, outputs_df, outputs_json, outputs_pdf, outputs_csv,
    outputs_excel]

# -------------------视频模式输出参数-------------------
outputs_video_list = [outputs_video, outputs_plot]

# 标题
title = "Gradio YOLOv5 Det v0.5"

# 描述
description = "<div align='center'>可自定义目标检测模型、安装简单、使用方便</div>"
# article="https://gitee.com/CV_Lab/gradio_yolov5_det"

# 示例图片
examples = [
    [
        "./img_examples/bus.jpg",
        "cpu",
        "yolov5s",
        640,
        0.6,
        0.5,
        10,
        ["人", "公交车"],
        ["label", "pdf"],],
    [
        "./img_examples/giraffe.jpg",
        "cuda:0",
        "yolov5l",
        320,
        0.5,
        0.45,
        12,
        ["长颈鹿"],
        ["label", "pdf"],],
    [
        "./img_examples/zidane.jpg",
        "cuda:0",
        "yolov5m",
        640,
        0.6,
        0.5,
        15,
        ["人", "领带"],
        ["pdf", "json"],],
    [
        "./img_examples/Millenial-at-work.jpg",
        "cuda:0",
        "yolov5s6",
        1280,
        0.5,
        0.5,
        20,
        ["人", "椅子", "杯子", "笔记本电脑"],
        ["label", "pdf"],],]

# 接口
gyd_img = gr.Interface(
    fn=yolo_det_img,
    inputs=inputs_img_list,
    outputs=outputs_img_list,
    title=title,
    description=description,
    # article=article,
    examples=examples,
    # theme="seafoam",
    # live=True, # 实时变更输出
    flagging_dir="run",  # 输出目录
    # allow_flagging="manual",
    # flagging_options=["good", "generally", "bad"],
)

gyd_video = gr.Interface(
    fn=yolo_det_video,
    inputs=inputs_video_list,
    outputs=outputs_video_list,
    title=title,
    description=description,
    # article=article,
    # examples=examples,
    # theme="seafoam",
    # live=True, # 实时变更输出
    flagging_dir="run",  # 输出目录
    allow_flagging="never",
    # flagging_options=["good", "generally", "bad"],
)

gyd = gr.TabbedInterface(interface_list=[gyd_img, gyd_video], tab_names=["图片模式", "视频模式"])

if __name__ == "__main__":
    if not is_login:
        gyd.launch(
            inbrowser=True,  # 自动打开默认浏览器
            show_tips=True,  # 自动显示gradio最新功能
            share=is_share,  # 项目共享，其他设备可以访问
            favicon_path="./icon/logo.ico",  # 网页图标
            show_error=True,  # 在浏览器控制台中显示错误信息
            quiet=True,  # 禁止大多数打印语句
        )
    else:
        gyd.launch(
            inbrowser=True,  # 自动打开默认浏览器
            show_tips=True,  # 自动显示gradio最新功能
            auth=usr_pwd,  # 登录界面
            share=is_share,  # 项目共享，其他设备可以访问
            favicon_path="./icon/logo.ico",  # 网页图标
            show_error=True,  # 在浏览器控制台中显示错误信息
            quiet=True,  # 禁止大多数打印语句
        )
